/*
 * Copyright (C) 2011 The Music Player Daemon Project
 * http://www.musicpd.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "config.h"
#include "fsevents_update.h"
#include "database.h"
#include "mapper.h"
#include "path.h"
#include "update.h"

#include <CoreServices/CoreServices.h>
#include <glib.h>

static FSEventStreamRef fs_stream;
static int fs_stream_is_initialized = 0;

struct fsevents_update_info {
	const char* const* event_paths;
	size_t num_events;
	GMutex *done_mutex;
	GCond *done_cond;
	int done;
};

static gboolean
fsevents_main_thread_callback(gpointer raw_info) {
	struct fsevents_update_info *info = raw_info;
	for (size_t i = 0; i < info->num_events; ++i) {
		g_debug("fsevents: queuing update for %s",
				info->event_paths[i]);
		update_enqueue(g_strdup(info->event_paths[i]), false);
	}
	g_mutex_lock(info->done_mutex);
	info->done = 1;
	g_mutex_unlock(info->done_mutex);
	g_cond_broadcast(info->done_cond);
	return FALSE;
}

static void
fsevents_callback(G_GNUC_UNUSED ConstFSEventStreamRef stream,
		G_GNUC_UNUSED void *user_data, size_t num_events,
		void *event_paths_raw,
		G_GNUC_UNUSED const FSEventStreamEventFlags *event_flags,
		G_GNUC_UNUSED const FSEventStreamEventId *event_ids)
{
	static struct fsevents_update_info info = { NULL, 0, NULL, NULL, 0 };
	if (!info.done_mutex) {
		info.done_mutex = g_mutex_new();
		info.done_cond = g_cond_new();
	}
	info.event_paths = event_paths_raw;
	info.num_events = num_events;
	info.done = 0;

	g_debug("fsevents: invoking callback in main thread");
	g_main_context_invoke(NULL, &fsevents_main_thread_callback, &info);
	g_mutex_lock(info.done_mutex);
	while (!info.done) {
		g_cond_wait(info.done_cond, info.done_mutex);
	}
	g_mutex_unlock(info.done_mutex);
	g_debug("fsevent: done invoking callback");
}

static GThread* run_loop_thread;
static GMutex* run_loop_mutex;
static GCond* run_loop_validated_cond;
static GCond* run_loop_ready_cond;
static int run_loop_valid = 0;
static int run_loop_ready = 0;
static CFRunLoopRef run_loop;

static gpointer
run_loop_thread_func(G_GNUC_UNUSED gpointer user_data) {
	g_mutex_lock(run_loop_mutex);
	run_loop = CFRunLoopGetCurrent();
	CFRetain(run_loop);
	run_loop_valid = 1;
	g_mutex_unlock(run_loop_mutex);
	g_cond_broadcast(run_loop_validated_cond);
	g_mutex_lock(run_loop_mutex);
	while (!run_loop_ready) {
		g_cond_wait(run_loop_ready_cond, run_loop_mutex);
	}
	g_mutex_unlock(run_loop_mutex);

	CFRunLoopRun();

	CFRelease(run_loop);

	return NULL;
}

static void
run_loop_init(void) {
	run_loop_mutex = g_mutex_new();
	run_loop_validated_cond = g_cond_new();
	run_loop_ready_cond = g_cond_new();

	if (!run_loop_mutex || !run_loop_validated_cond) {
		g_error("couldn't create run_loop mutex or cond");
	}

	run_loop_thread = g_thread_create(&run_loop_thread_func,
			NULL, false, NULL);
	if (!run_loop_thread) {
		g_error("couldn't create run loop thread");
	}

	g_thread_yield();
	g_mutex_lock(run_loop_mutex);
	while (!run_loop_valid) {
		g_cond_wait(run_loop_validated_cond, run_loop_mutex);
	}
	g_mutex_unlock(run_loop_mutex);
}

void
mpd_fsevents_init(void)
{
	struct directory *root;
	char *path_fs;
	char *path_utf8;
	int success;

	root = db_get_root();
	if (root == NULL) {
		g_warning("fsevents: no music directory configured");
		return;
	}

	path_fs = map_directory_fs(root);
	if (path_fs == NULL) {
		g_warning("fsevents: map_directory_fs failed");
		return;
	}

	path_utf8 = fs_charset_to_utf8(path_fs);
	if (path_utf8 == NULL) {
		path_utf8 = path_fs;
	} else {
		g_free(path_fs);
	}

	CFStringRef path_as_cfstring = CFStringCreateWithCString(NULL,
			path_utf8, kCFStringEncodingUTF8);
	CFArrayRef path_as_cfarray = CFArrayCreate(NULL,
			(const void **) &path_as_cfstring, 1, NULL);

	fs_stream = FSEventStreamCreate(kCFAllocatorDefault,
			&fsevents_callback,
			NULL,
			path_as_cfarray, kFSEventStreamEventIdSinceNow,
			5.0, /* event notification latency in seconds */
			kFSEventStreamCreateFlagNone);

	run_loop_init();

	FSEventStreamScheduleWithRunLoop(fs_stream, run_loop,
			kCFRunLoopDefaultMode);

	success = FSEventStreamStart(fs_stream);
	if (!success) {
		g_error("FSEventStreamStart failed");
	}

	g_mutex_lock(run_loop_mutex);
	run_loop_ready = 1;
	g_mutex_unlock(run_loop_mutex);
	g_cond_broadcast(run_loop_ready_cond);

	fs_stream_is_initialized = 1;

	g_debug("fsevents: initialized (for %s)", path_utf8);
}

void
mpd_fsevents_finish(void)
{
	if (fs_stream_is_initialized) {
		FSEventStreamStop(fs_stream);
		FSEventStreamInvalidate(fs_stream);
		FSEventStreamRelease(fs_stream);
		CFRunLoopStop(run_loop);
		run_loop = NULL;
		run_loop_valid = 0;
		fs_stream_is_initialized = 0;
	}
}
